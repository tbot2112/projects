# Name this file solutions.py.
"""Volume II Lab 10: Fourier II (Filtering and Convolution)
Tanner Thompson
Math 321
11/12/15
"""
import scipy as sp
from scipy.io import wavfile
import numpy as np
from matplotlib import pyplot as plt

# Problem 1: Implement this function.
def clean_signal(outfile='prob1.wav'):
    """Clean the 'Noisysignal2.wav' file. Plot the resulting sound
    wave and write the resulting sound to the specified outfile.
    """
    rate, data = wavfile.read('Noisysignal2.wav')
    fsig = sp.fft(data, axis = 0)
    
    for j in xrange(14700, 50000):
        fsig[j] = 0
        fsig[-j] = 0
    
    x = np.arange(1,len(fsig)+1, 1) * 1.
    x /= len(fsig)
    x *= rate*2
    plt.plot(x, fsig)
    # plt.plot(fsig)
    plt.axis([0,22050,0,fsig.max()])
    plt.show()
    
    newsig = sp.ifft(fsig)
    newsig = sp.real(newsig)
    newsig = sp.int16(newsig / sp.absolute(newsig).max() * 32767)
    
    wavfile.write(outfile, rate, newsig)

# Problem 2 is not required. Use balloon.wav for problem 3.

# Problem 3: Implement this function.
def convolve(source='chopin.wav', pulse='balloon.wav', outfile='prob3.wav'):
    """Convolve the specified source file with the specified pulse file, then
    write the resulting sound wave to the specified outfile.
    """
    
    sourcerate, sourcedata = wavfile.read(source)
    pulserate, pulsedata = wavfile.read(pulse)
    
    sourcedata = sourcedata[:,0]
    pulsedata = pulsedata[:,0]
    
    sourcedata = np.concatenate((sourcedata, np.zeros(44100*5)))
    
    l = len(pulsedata)
    add = len(sourcedata) - l
    pulsedata = np.concatenate((pulsedata[:l/2], np.zeros(add), pulsedata[l/2:]))
    
    sourcet = sp.fft(sourcedata)
    pulset = sp.fft(pulsedata)

    result = sp.ifft(sourcet*pulset)
    result = sp.real(result)
    result = sp.int16(result / sp.absolute(result).max() * 32767)

    wavfile.write(outfile, sourcerate, result)


# Problem 4: Implement this function.
def white_noise(outfile='prob4.wav'):
    """Generate some white noise, write it to the specified outfile,
    and plot the spectrum (DFT) of the signal.
    """
    samplerate = 44100
    noise = sp.int16(sp.random.randint(-32767, 32767, samplerate*10))

    wavfile.write(outfile, samplerate, noise)

    transform = sp.fft(noise)
    x = np.arange(1,len(transform)+1, 1) * 1.
    x /= len(noise)
    x *= samplerate*2
    plt.plot(x, transform)
    plt.axis([0,22050,0,transform.max()])
    plt.show()