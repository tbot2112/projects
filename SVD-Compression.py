# solutions.py
from scipy import linalg as la
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm

# Problem 1
def truncated_svd(A,r=None,tol=10**-6):
    """Computes the truncated SVD of A. If r is None or equals the number 
        of nonzero singular values, it is the compact SVD.
    Parameters:
        A: the matrix
        r: the number of singular values to use
        tol: the tolerance for zero
    Returns:
        U - the matrix U in the SVD
        s - the diagonals of Sigma in the SVD
        Vh - the matrix V^H in the SVD
    """
    m, n = A.shape
    e, V = la.eig(A.conj().T.dot(A))
    order = np.argsort(e)
    e = e[order][::-1]
    V = V[:,order][:,::-1]
    s = np.sqrt(e)
    s[s<tol] = 0
    
    if r is None:
        try:
            r = np.argwhere(s==0)[0,0]   
            s = s[:r] 
            V = V[:,:r]
        except:
            r = len(s)
    
    elif type(r) is int and r > 0:
        s = s[:r]
        V = V[:,:r]
        
    else:
        raise TypeError("your r was not a valid integer")
    
    U = np.zeros((m,r)).astype(complex)
    for i in range(r):
        U[:,i] = A.dot(V[:,i])/s[i]
        
    # print U.dot(np.diag(s)).dot(V.T.conj())
    return U, s, V.T.conj()
    

# Problem 2
def visualize_svd():
    """Plot each transformation associated with the SVD of A."""
    unit_vectors = np.load('circle.npz')['unit_vectors']
    circle = np.load('circle.npz')['circle']
    U, s, VH = truncated_svd(np.array([[3,1],[1,3]]))
    
    plt.subplot(2,2,1)
    plt.plot(unit_vectors[0], unit_vectors[1])
    plt.plot(circle[0], circle[1])
    
    plt.subplot(2,2,2)
    unit_vectors = VH.dot(unit_vectors)
    circle = VH.dot(circle)
    plt.plot(unit_vectors[0], unit_vectors[1])
    plt.plot(circle[0], circle[1])
    
    plt.subplot(2,2,3)
    unit_vectors = np.diag(s).dot(unit_vectors)
    circle = np.diag(s).dot(circle)
    plt.plot(unit_vectors[0], unit_vectors[1])
    plt.plot(circle[0], circle[1])
    
    plt.subplot(2,2,4)
    unit_vectors = U.dot(unit_vectors)
    circle = U.dot(circle)
    plt.plot(unit_vectors[0], unit_vectors[1])
    plt.plot(circle[0], circle[1])
    
    plt.show()
    

# Problem 3
def svd_approx(A, k):
    """Returns best rank k approximation to A with respect to the induced 2-norm.
    
    Inputs:
    A - np.ndarray of size mxn
    k - rank 
    
    Return:
    Ahat - the best rank k approximation
    """
    
    U, s, VH = truncated_svd(A, k)
    return U.dot(np.diag(s)).dot(VH)
    
# Problem 4
def lowest_rank_approx(A,e):
    """Returns the lowest rank approximation of A with error less than e 
    with respect to the induced 2-norm.
    
    Inputs:
    A - np.ndarray of size mxn
    e - error
    
    Return:
    Ahat - the lowest rank approximation of A with error less than e.
    """
    U, s, VH = la.svd(A, full_matrices=False)
    s[s<e] = 0
    return U.dot(np.diag(s)).dot(VH)
    
    
# Problem 5
def compress_image(filename, k):
    """Plot the original image found at 'filename' and the rank k approximation
    of the image found at 'filename.'
    
    filename - jpg image file path
    k - rank
    """

    R = plt.imread(filename)[:,:,0].astype(float)
    G = plt.imread(filename)[:,:,1].astype(float)
    B = plt.imread(filename)[:,:,2].astype(float)
    
    Rnew = svd_approx(R, k).astype(int)
    Rnew[Rnew > 255] = 255
    Rnew[Rnew < 0] = 0
    Rnew = 255 - Rnew
    # Rnew /= 255.
    Gnew = svd_approx(G, k).astype(int)
    Gnew[Gnew > 255] = 255
    Gnew[Gnew < 0] = 0
    Gnew = 255 - Gnew
    # Gnew /= 255.
    Bnew = svd_approx(B, k).astype(int)
    Bnew[Bnew > 255] = 255
    Bnew[Bnew < 0] = 0
    Bnew = 255 - Bnew
    # Bnew /= 255.
    

    plt.imshow(np.transpose(np.array([Rnew, Gnew, Bnew]),[1,2,0]))
    plt.show()

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    