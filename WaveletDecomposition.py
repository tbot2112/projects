# name this file solutions.py
"""Volume II Lab 11: Wavelets
Tanner Thompson
Math 321
12/2/2015
"""
from scipy.signal import fftconvolve
import numpy as np
from matplotlib import pyplot as plt

# Problem 1: Implement this AND the following function.
def dwt(X, L, H, n):
    """Compute the Discrete Wavelet Transform of X using filters L and H.

    Parameters:
        X (1D ndarray): The signal to be processed.
        L (1D ndarray): The low-pass filter.
        H (1D ndarray): The high-pass filter.
        n (int > 0): Controls the degree of transformation.
    
    Returns:
        a list of the wavelet decomposition arrays.
    """
    i = 0
    A = X
    D = []
    while i < n:
        D.append(fftconvolve(A,H)[1::2])
        A = fftconvolve(A,L)[1::2]
        i += 1
    D.append(A)
    return D[::-1]
    
    

def plot(X, L, H, n): # repair so it plot for arbitrary n, also plot X at first
    """Plot the results of dwt with the given inputs.
    Your plot should be very similar to Figure 2.
    
    Parameters:
        X (1D ndarray): The signal to be processed.
        L (1D ndarray): The low-pass filter.
        H (1D ndarray): The high-pass filter.
        n (int > 0): Controls the degree of transformation.
    """
    
    coeffs = dwt(X, L, H, n)
    print len(coeffs)
    
    plt.subplot(n+2,1,1)
    plt.plot(X)
    
    for i in range(0,len(coeffs)):
        plt.subplot(n+2, 1, i+2)
        plt.plot(coeffs[i])
    
    # A4, D4, D3, D2, D1 = coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4]
    
    # plt.subplot(5,1,1)
    # plt.plot(np.linspace(0,1,len(A4)), A4)
    # plt.subplot(5,1,2)
    # plt.plot(np.linspace(0,1,len(D4)), D4)
    # plt.subplot(5,1,3)
    # plt.plot(np.linspace(0,1,len(D3)), D3)
    # plt.subplot(5,1,4)
    # plt.plot(np.linspace(0,1,len(D2)), D2)
    # plt.subplot(5,1,5)
    # plt.plot(np.linspace(0,1,len(D1)), D1)
    
    plt.show()

# Problem 2: Implement this function.
def idwt(coeffs, L, H):
    """
    Parameters:
        coeffs (list): a list of wavelet decomposition arrays.
        L (1D ndarray): The low-pass filter.
        H (1D ndarray): The high-pass filter.
    Returns:
        The reconstructed signal (as a 1D ndarray).
    """
    coeffs = coeffs[::-1]
    A = coeffs.pop()
    i = len(coeffs)
    while i > 0:
        D = coeffs.pop()
        up_A = np.zeros(2*A.size)
        up_A[::2] = A
        up_D = np.zeros(2*D.size)
        up_D[::2] = D
        
        A = fftconvolve(up_A,L)[:-1] + fftconvolve(up_D,H)[:-1]
        i -= 1
    return A






