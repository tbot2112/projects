# solutions.py

import numpy as np
from numpy import linalg as la

# Problem 1: Implement this function.
def centered_difference_quotient(f,pts,h = 1e-5):
    '''
    Compute the centered difference quotient for function (f)
    given points (pts).
    Inputs:
        f (function): the function for which the derivative will be approximated
        pts (array): array of values to calculate the derivative
    Returns:
        centered difference quotient (array): array of the centered difference
            quotient
    '''
    pts = np.array(pts)
    return (f(pts+h) - f(pts-h))/(2*h)


# Problem 2: Implement this function.
def jacobian(f,n,m,pt,h = 1e-5):
    '''
    Compute the approximate Jacobian matrix of f at pt using the centered
    difference quotient.
    Inputs:
        f (function): the multidimensional function for which the derivative
            will be approximated
        n (int): dimension of the domain of f
        m (int): dimension of the range of f
        pt (array): an n-dimensional array representing a point in R^n
        h (float): a float to use in the centered difference approximation
    Returns:
        Jacobian matrix of f at pt using the centered difference quotient.
    '''
    J = np.zeros((m,n))
    pt = np.array(pt)
    for j in range(n):

        hpt1 = pt.copy()
        hpt2 = pt.copy()
        hpt1[j] += h
        hpt2[j] -= h
        J[:,j] = (f(hpt1) - f(hpt2))/(2*h)
    return J
            
    


# Problem 3: Implement this function.
def findError():
    '''
    Compute the maximum error of your jacobian function for the function
    f(x,y)=[(e^x)*sin(y)+y^3,3y-cos(x)] on the square [-1,1]x[-1,1].
    Returns:
        Maximum error of your jacobian function.
    '''
    f = lambda (x,y): np.array([np.exp(x)*np.sin(y) + y**3, 3*y - np.cos(x)])
    J = lambda (x,y): np.array([[np.exp(x)*np.sin(y), np.exp(x)*np.cos(y) + 3*y**2],[np.sin(x),3]])
    errorM = np.zeros((100,100))
    for i in range(100):
        for j in range(100):
            x = i/50. - 1
            y = j/50. - 1
            thisJ = J((x,y))
            otherJ = jacobian(f,2,2,(x,y))
            errorM[i,j] = la.norm(thisJ - otherJ)
            
    return np.max(errorM)
    
    
        
# Problem 4: Implement this function.
def Filter(image,F):
    '''
    Applies the filter to the image.
    Inputs:
        image (array): an array of the image
        F (array): an nxn filter to be applied (a numpy array).
    Returns:
        The filtered image.
    '''
    print image.shape, 'is the shape of the image'
    m, n = image.shape
    h, k = F.shape
    image_pad = np.zeros((m+h-1,n+k-1))
    image_pad[h//2:h//2+m,k//2:k//2+n] = image
    
    C = np.zeros(image.shape)
    for i in range(m):
        for j in range(n):
            C[i,j] = sum([sum([F[kk,l]*image_pad[i+kk,j+l] for l in range(-k//2,k//2+1)]) for kk in range(-h//2, h//2+1)])

    return C

# Problem 5: Implement this function.
def sobelFilter(image):
    '''
    Applies the Sobel filter to the image
    Inputs:
        image(array): an array of the image in grayscale
    Returns:
        The image with the Sobel filter applied.
    '''
    S = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])/8.
    
    AS = Filter(image, S)
    AST = Filter(image, S.T)
    
    m,n = image.shape
    delA = np.zeros((m,n))
    for i in range(m):
        for j in range(n):
            delA[i,j] = np.sqrt(AS[i,j]**2 + AST[i,j]**2)
            
    M = delA.mean()
    
    return (delA > 4*M).astype(int)
    