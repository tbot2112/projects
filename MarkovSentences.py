# spec.py
"""Volume II Lab 8: Markov Chains
Tanner Thompson
Math 321
10/26/15
"""
import numpy as np

# Problem 1: implement this function.
def random_markov(n):
    """Create and return a transition matrix for a random
    Markov chain with 'n' states as an nxn NumPy array.
    """
    a = np.random.rand(n,n)
    for i in range(n):
        a[i] /= sum(a[i])
    
    return a.T


# Problem 2: modify this function.
def forecast(num_days):
    """Run a simulation for the weather over 'num_days' days, with
    "hot" as the starting state. Return a list containing the day-by-day
    results, not including the starting day.

    Example:
        >>> forecast(5)
        [1, 0, 0, 1, 0]
        # Or, if you prefer,
        ['cold', 'hot', 'hot', 'cold', 'hot']
    """
    transition_matrix = np.array([[0.7, 0.6], [0.3, 0.4]])
    state = 0
    prediction = []
    for i in range(num_days):
        random_number = np.random.random()
        if random_number < transition_matrix[1,state]:
            prediction.append(1)
            state = 1 
        else:
            prediction.append(0)
            state = 0
    return prediction


# Problem 3: implement this function.
def four_state_forecast(days=1):
    """Same as forecast(), but using the four-state transition matrix."""
    transition_matrix = np.array([[.5,.3,.1,0],[.3,.3,.2,.3],[.2,.3,.4,.5],[0,.1,.2,.2]])
    prediction = []
    state = 0
    for i in range(days):
        a = transition_matrix[:,state]
        mult = np.random.multinomial(1, a)
        state = np.where(mult == 1)[0][0]
        prediction.append(state)
    return prediction

# Problem 4: implement this function.
def analyze_simulation(n=100000):
    """Analyze the results of the previous two problems. What percentage
    of days are in each state? Print your results to the terminal.
    """
    fsf = four_state_forecast(n)
    fsfresults = np.bincount(fsf)
    d = {0:'hot', 1:'mild', 2:'cold', 3:'freezing'}
    print "in the four-state forecast:"
    for i in {0,1,2,3}:
        print str(100*fsfresults[i]/n) + "% of all days were " + d[i]
        
    dd = {0:'hot', 1:'cold'}
    tsf = forecast(n)
    tsfresults = np.bincount(tsf)
    print '\nin the two-state forecast:'
    for i in {0,1}:
        print str(100*tsfresults[i]/n) + "% of all days were " + dd[i]
        
    


# Problems 5-6: define and implement the described functions.
def convert_to_numbers(infile, outfile):
    """Convert the words in infile to numbers and write them to outfile, preserving line break structure.
    
    Parameters:
        infile (str): the path to a file containing a training set of words
        outfile (str): the path to the file to write the resulting numbers to
    
    Returns:
        words (str): a list of strings, sorted by the number used to code them, plus unique start
            and end values
    """
    words = ['$tart']
    with open(infile, 'r') as readfile:
        contents = readfile.readlines()
    
    writefile = open(outfile, 'w')
    
    for line in contents:
        for word in line.split():
            if word not in words:
                words.append(word)
            index = words.index(word)
            writefile.write(str(index) + ' ')
        writefile.write('\n')
    
    words.append('en&')
    return words
    
def transition_matrix(sentencefile, num_words):
    """Creates the transition matrix for the markov chain corresponding to the data in wordfile.
    
    Parameters:
        sentencefile (str): the path to the file created by convert_to_numbers
        num_words (int): the number of unique words in the text
        
    Returns:
        transition_matrix (numpy.ndarray): the transition matrix represented by the data from wordfile
    """
    transition_matrix = np.zeros((num_words + 2, num_words + 2))
    with open(sentencefile, 'r') as readfile:
        contents = readfile.readlines()
    
    for line in contents:
        nums = line.split()
        if len(nums) != 0:
            transition_matrix[nums[0], 0] += 1
            for i in xrange(len(nums)):
                if i == len(nums) - 1:
                    transition_matrix[num_words+1,nums[-1]] += 1
                else:
                    transition_matrix[nums[i+1], nums[i]] += 1
        
    transition_matrix /= transition_matrix.sum(axis = 0)
    return transition_matrix
    


# Problem 7: implement this function.
def sentences(infile, outfile, num_sentences=1):
    """Generate random sentences using the word list generated in
    Problem 5 and the transition matrix generated in Problem 6.
    Write the results to the specified outfile.

    Parameters:
        infile (str): The path to a filen containing a training set.
        outfile (str): The file to write the random sentences to.
        num_sentences (int): The number of random sentences to write.

    Returns:
        None
    """
    words = convert_to_numbers(infile, 'temp.txt')
    trans = transition_matrix('temp.txt', len(words) - 2)
    
    writefile = open(outfile, 'w')
    
    for i in xrange(num_sentences):
        state = 0
        while state != len(words) - 1:
            a = trans[:, state]
            mult = np.random.multinomial(1, a)
            state = np.where(mult == 1)[0][0]
            if state != len(words) - 1:
                writefile.write(words[state] + ' ')
        writefile.write('\b.\n')
        
            
        
    
